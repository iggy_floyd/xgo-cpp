
package main

/*

#cgo CFLAGS: -I /usr/local/include
#include "xgo-cpp/interface.h"
#cgo LDFLAGS: -L /usr/local/lib/xgo-cpp -lxgo-cpp  -lstdc++

*/
import "C"

func GoRandom() int {
	return int(C.CRandom())
}
