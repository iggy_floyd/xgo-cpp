
# @ Igor Marfin <Unister Gmb, 2014> igor.marfin@unister.de
# A simple makefile to manage this project
SHELL = /bin/bash
PKGNAME  = $(shell basename $(shell pwd))
PREFIX = /usr/local

all: build


build: clean
	-@./compile.sh
		


install: build
	-echo $(PKGNAME)
	-@mkdir -p $(PREFIX)/lib/$(PKGNAME)
	-@mkdir -p $(PREFIX)/include/$(PKGNAME)
	-@cp -r  lib/* $(PREFIX)/lib/$(PKGNAME)
	-@cp -r  include/* $(PREFIX)/include/$(PKGNAME)

uninstall:
	-@rm -rf $(PREFIX)/lib/$(PKGNAME)
	-@rm -rf $(PREFIX)/include/$(PKGNAME)


clean:
	-@rm -rf lib
	-@rm $(PKGNAME)
	-@rm $(PKGNAME).tar.bz2
	-@rm .arch

build_prog:
	-@go build --ldflags '-extldflags "-static"'

build_pkg:
	-@cd ..; tar -cvjSf $(PKGNAME).tar.bz2 --exclude $(PKGNAME)/.git $(PKGNAME); mv  $(PKGNAME).tar.bz2  $(PKGNAME)

.PHONY: all build install uninstall clean build_prog

